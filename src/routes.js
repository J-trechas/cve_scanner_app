import DataCollectorView from "views/DataCollector";
import SoftwareCollectorView from "views/SoftwareCollector";
import ExploitCollectorView from "views/ExploitCollector";
import UpdateDataView from "views/UpdateData";
import ScannerView from "views/Scanner";
import DatabaseQueriesView from "views/DatabaseQueriesView";
import CVEListView from "views/CVEListView";
import SettingsView from "views/SettingsView";
import HelpView from "views/Help";

const dashboardRoutes = [
  {
    path: "/dataCollector",
    name: "CWE-CVE Collector",
    //icon: "nc-icon nc-chart-pie-35",
    component: DataCollectorView,
    layout: "/admin"
  },
  {
    path: "/updateData",
    name: "Update CVE Data",
    //icon: "nc-icon nc-chart-pie-35",
    component: UpdateDataView,
    layout: "/admin"
  },
  {
    path: "/exploitCollector",
    name: "Exploit Collector",
    //icon: "nc-icon nc-chart-pie-35",
    component: ExploitCollectorView,
    layout: "/admin"
  },
  {
    path: "/cves",
    name: "CVE-Exploits List",
    //icon: "nc-icon nc-chart-pie-35",
    component: CVEListView,
    layout: "/admin"
  },
  {
    path: "/softwareCollector",
    name: "Software Collector",
    //icon: "nc-icon nc-chart-pie-35",
    component: SoftwareCollectorView,
    layout: "/admin"
  },
  {
    path: "/scanner",
    name: "Scan for vulnerabilities",
    //icon: "nc-icon nc-chart-pie-35",
    component: ScannerView,
    layout: "/admin"
  },
  {
    path: "/queries",
    name: "Database Queries",
    //icon: "nc-icon nc-chart-pie-35",
    component: DatabaseQueriesView,
    layout: "/admin"
  },
  {
    path: "/settings",
    name: "Settings",
    //icon: "nc-icon nc-chart-pie-35",
    component: SettingsView,
    layout: "/admin"
  },
  {
    path: "/help",
    name: "Help",
    //icon: "nc-icon nc-chart-pie-35",
    component: HelpView,
    layout: "/admin"
  }
];

export default dashboardRoutes;
