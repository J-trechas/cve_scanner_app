import React, { useState } from "react";
import { Button, Card, Table } from "react-bootstrap";
import { CSVLink } from "react-csv";


const styles = {
  buttonContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
    marginBottom: 15,
  },
};

const CustomTable = ({ tableData, rowsToLoad, cvelink }) => {
  const initialRows = rowsToLoad || 10
  const [displayedRows, setDisplayedRows] = useState(initialRows);
  const [sortConfig, setSortConfig] = useState({ key: null, direction: null });

  const handleLoadMore = () => {
    setDisplayedRows(displayedRows + initialRows);
  };


  const onHeaderClick = (key) => {
    let direction = 'ascending';
    if (sortConfig.key === key && sortConfig.direction === 'ascending') {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  };

  const sortedData = React.useMemo(() => {
    let sortableData = [...tableData];
    if (sortConfig !== null) {
      sortableData.sort((a, b) => {
        // Check if values are numeric
        if (!isNaN(a[sortConfig.key]) && !isNaN(b[sortConfig.key])) {
          // If values are numeric, convert to number before comparing
          if (Number(a[sortConfig.key]) < Number(b[sortConfig.key])) {
            return sortConfig.direction === 'ascending' ? -1 : 1;
          }
          if (Number(a[sortConfig.key]) > Number(b[sortConfig.key])) {
            return sortConfig.direction === 'ascending' ? 1 : -1;
          }
        } else {
          // If values are not numeric, compare as strings
          if (a[sortConfig.key] < b[sortConfig.key]) {
            return sortConfig.direction === 'ascending' ? -1 : 1;
          }
          if (a[sortConfig.key] > b[sortConfig.key]) {
            return sortConfig.direction === 'ascending' ? 1 : -1;
          }
        }
        return 0;
      });
    }
    return sortableData;
  }, [tableData, sortConfig]);


  return (
    <Card className="card-plain table-plain-bg">
      <Card.Body className="table-full-width table-responsive px-0">
        <CSVLink data={sortedData} className="btn btn-info ml-2" filename={`${new Date().toLocaleDateString('en-CA')}-db.csv`}>Export to CSV</CSVLink>
        <Table className="table-hover">
          <thead>
            <tr>
              {Object.keys(tableData[0]).map((columnName) => (
                <th className="border-0" key={columnName}>
                  <Button variant="link" style={{ outline: 'none', border: 'none', display: 'flex', width: '100%', padding: 0 }} onClick={() => onHeaderClick(columnName)}>
                    {columnName}
                  </Button>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {sortedData.slice(0, displayedRows).map((row, index) => (
              <tr key={index}>
                {Object.keys(tableData[0]).map((columnName) => {
                  let cellValue = row[columnName];
                  let cellStyle = { maxWidth: '300px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' };
                  if (columnName === 'severity') {
                    if (cellValue === 'Critical') {
                      cellStyle = { maxWidth: '300px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', backgroundColor: 'red', color: 'white' };
                    } else if (cellValue === 'High') {
                      cellStyle = { maxWidth: '300px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', backgroundColor: 'orange', color: 'white' };
                    } else if (cellValue === 'Medium') {
                      cellStyle = { maxWidth: '300px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', backgroundColor: 'yellow' };
                    } else if (cellValue === 'Low') {
                      cellStyle = { maxWidth: '300px', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', backgroundColor: 'green' };
                    }
                  }
                  return (
                    <td key={columnName} style={cellStyle} title={row[columnName]}>
                      {cvelink && columnName === "id" && row[columnName].startsWith("CVE") ? (
                        <a
                          href={`https://nvd.nist.gov/vuln/detail/${row[columnName]}`}
                          /* href={`https://cve.mitre.org/cgi-bin/cvename.cgi?name=${row[columnName]}`} */
                          target="_blank"
                          rel="noopener noreferrer"
                          style={{ minWidth: '100px' }}
                        >
                          {cellValue}
                        </a>
                      ) : (
                        columnName === "exploit_file" ?
                          (<a
                            href={`https://gitlab.com/exploit-database/exploitdb/-/tree/main/${row[columnName]}`}
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{ minWidth: '100px' }}
                          >
                            {cellValue}
                          </a>
                          ) : (
                            cellValue
                          )
                      )}
                    </td>
                  );
                })}
              </tr>
            ))}
          </tbody>
        </Table>
        {tableData.length > displayedRows && (
          <div style={styles.buttonContainer}>
            <Button
              onClick={handleLoadMore}
              className="btn-fill pull-right"
              type="submit"
              variant="info"
            >
              Show more
            </Button>
          </div>
        )}
      </Card.Body>
    </Card>
  );
};

export default CustomTable;
