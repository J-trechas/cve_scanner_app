import React, { Component } from "react";
import { Container } from "react-bootstrap";

class Footer extends Component {
  render() {
    return (
      <footer className="footer px-0 px-lg-3">
        <Container fluid>
          <nav style={{display: 'flex', justifyContent: 'space-between'}}>
            <p style={{fontSize:12,margin:0,alignSelf:'center'}}>This product uses the NVD API but is not endorsed or certified by the NVD</p>
            <p className="copyright text-center">
              © {new Date().getFullYear()}{" "}
              <a href="https://www.linkedin.com/in/ioannis-trechas-9a3a16225/" target="_blank">Ioannis Trechas</a>
            </p>
          </nav>
        </Container>
      </footer>
    );
  }
}

export default Footer;
