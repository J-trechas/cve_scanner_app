import React, { useEffect, useRef, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { getLocalInstalledSoftware } from "code/db";
import CustomTable from "components/Table";
import { getInstalledSoftwareDB } from "code/db";
import { UI_MESSAGES } from "code/AppConstants";

const styles = {
    button: {
        marginBottom: 15
    },
    logText: {
        fontSize: 10,
        height: 100
    }
}

const SoftwareCollectorView = props => {
    const logContainerRef = useRef(null);
    const [outputText, setOutputText] = useState('');
    const [disabled, setDisabled] = useState(false)
    const [tableData, setTableData] = useState([])

    useEffect(() => {
        logContainerRef.current.scrollTop = logContainerRef.current.scrollHeight;
    }, [outputText]);

    // Function to run the script and set the output text state
    const runScript = async () => {
        setDisabled(true)
        setOutputText(UI_MESSAGES.SOFTWARE_COLLECTOR_INITIAL_MSG)
        setTableData([])
        const originalLog = console.log;
        console.log = (message) => {
            setOutputText((prevOutput) => prevOutput + message + '\n');
            originalLog(message);
        };

        await getLocalInstalledSoftware(true)
        const apps = await getInstalledSoftwareDB();
        setTableData(apps.results.map(app => {
            const clone = { ...app }
            clone.entryDate = app.entryDate.slice(0, 10)
            clone.lastModified = app.lastModified.slice(0, 10)
            return clone
        }))
        setOutputText(out => out +UI_MESSAGES.APPS_FOUND_NUMBER.replace('{number}',apps.results.length));
        setDisabled(false)
        // Restore original console.log function
        console.log = originalLog;
    };


    return (
        <Container fluid>
            <div className="output-box">
                <Button
                    onClick={runScript}
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    style={styles.button}
                    disabled={disabled}
                >
                    {UI_MESSAGES.BUTTON_COLLECT}
                </Button>
                <Form.Group>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        value={outputText}
                        style={styles.logText}
                        ref={logContainerRef}
                    ></Form.Control>
                </Form.Group>
            </div>
            {
                tableData.length > 0 &&
                (<CustomTable tableData={tableData} rowsToLoad={tableData.length} />)
            }
        </Container>
    )

}

export default SoftwareCollectorView