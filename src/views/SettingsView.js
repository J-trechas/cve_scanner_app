import { UI_MESSAGES } from 'code/AppConstants';
import { executeQuery } from 'code/db';
import React, { useState, useEffect, useRef } from 'react';
import { Alert, Button, Container, Form, Modal, } from 'react-bootstrap';
import Checkbox from 'react-custom-checkbox';
import { Input, Label } from 'reactstrap';


const styles = {
    button: {
        marginBottom: 15
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'start',
        marginBottom: 20
    },
    label: {
        marginBottom: 0,
        marginRight: '10px',
        fontWeight: 'bold'
    },
    logText: {
        width: '100%',
        height: 300,
        fontSize: 11
    }
}

const SettingsView = () => {
    const [showModal, setShowModal] = useState(false);
    const [check_cwe, setCheckCWE] = useState(false);
    const [check_cve, setCheckCVE] = useState(false);
    const [check_products, setCheckProducts] = useState(false);
    const [check_exploits, setCheckExploits] = useState(false);
    const [check_installedsoftware, setCheckInstalledSoftware] = useState(false);
    const [errorText, setErrorText] = useState("")
    const [successText, setSuccessText] = useState("")
    const [logText, setLogText] = useState("")
    const fileInput = useRef();



    const handleUpload = () => {
        setSuccessText("")
        setErrorText("")
        const file = fileInput.current.files[0];

        if (file) {
            const formData = new FormData();
            formData.append('dbDump', file);

            fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/uploadDB`, {
                method: 'POST',
                body: formData,
            })
                .then(response => {
                    if (response.ok) {
                        setSuccessText(UI_MESSAGES.SUCCESS_IMPORT);
                    } else {
                        setErrorText(UI_MESSAGES.ERROR_IMPORT);
                    }
                })
        } else {
            setErrorText(UI_MESSAGES.WARNING_NO_FILE_SELECTED);
        }
    }


    const deleteFromTables = () => {
        let tablesToDeleteFrom = [];

        if (check_cwe) tablesToDeleteFrom.push('cwe');
        if (check_cve) tablesToDeleteFrom.push('cve');
        if (check_products) tablesToDeleteFrom.push('products');
        if (check_exploits) tablesToDeleteFrom.push('exploits');
        if (check_installedsoftware) tablesToDeleteFrom.push('installedsoftware');

        let queries = tablesToDeleteFrom.map(table => `DELETE FROM ${table};`);

        return queries;
    }

    return (
        <Container fluid>
            {
                successText &&
                <Alert variant="success">
                    <button
                        aria-hidden={true}
                        className="close"
                        data-dismiss="alert"
                        type="button"
                        onClick={() => { setSuccessText("") }}
                    >
                        <i className="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                        {successText}
                    </span>
                </Alert>
            }
            {
                errorText &&
                <Alert variant="danger">
                    <button
                        aria-hidden={true}
                        className="close"
                        data-dismiss="alert"
                        type="button"
                        onClick={() => { setErrorText("") }}
                    >
                        <i className="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                        {errorText}
                    </span>
                </Alert>
            }
            <span style={{ display: 'inline-block', backgroundColor: '#f8f9fa', paddingBottom: '10px', borderRadius: '5px', color: '#6c757d', fontSize: 11 }}>{UI_MESSAGES.DEFAULT_DOWNLOAD_LOCATION_NOTE}</span>

            <div className="output-box" style={styles.container}>
                <Label style={styles.label}>{UI_MESSAGES.IMPORT_LABEL}</Label>
                <Input type="file" innerRef={fileInput} accept=".sql" style={{ width: 'auto' }} />
                <Button
                    onClick={handleUpload}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_IMPORT}
                </Button>
            </div>
            <div className="output-box" style={styles.container}>
                <Label style={styles.label}>{UI_MESSAGES.EXPORT_LABEL}</Label>
                <Button
                    onClick={() => {
                        window.open(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/downloadDB`);
                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_EXPORT}
                </Button>
                <span style={{ display: 'inline-block', backgroundColor: '#f8f9fa', padding: '10px', borderRadius: '5px', color: '#6c757d', fontSize: 11 }}>{UI_MESSAGES.PATH_WARNING_NOTE}</span>
            </div>
            <div className="output-box" style={styles.container}>
                <Label style={styles.label}>{UI_MESSAGES.LOG_LABEL}</Label>
                <Button
                    onClick={() => {
                        window.open(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/log`);
                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DOWNLOAD}
                </Button>
                <Button
                    onClick={() => {

                        if (!logText) {
                            setErrorText("")
                            setSuccessText("")

                            fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/log_raw`)
                                .then(response => response.json())
                                .then(data => {
                                    if (data.error) {
                                        setErrorText(UI_MESSAGES.ERROR_LOG + data.error);

                                    } else {

                                        setSuccessText(UI_MESSAGES.SUCCESS_LOG_RETRIEVED);
                                        setLogText(data.log)
                                    }
                                })
                                .catch(err => {
                                    setErrorText(UI_MESSAGES.ERROR_LOG_RETRIEVAL, err);
                                });
                        }
                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DISPLAY}
                </Button>
            </div>
            {logText &&
                <Form.Group>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        defaultValue={logText}
                        style={styles.logText}
                        readOnly
                    ></Form.Control>
                </Form.Group>
            }
            <div className="output-box mt-3">
                <Label style={styles.label}>{UI_MESSAGES.DELETE_TABLE_LABEL}</Label>
                <div className="m-2" style={{ display: 'flex', flexDirection: 'column' }}>
                    <Checkbox
                        name="cwe"
                        label="cwe"
                        checked={check_cwe}
                        onChange={() => setCheckCWE(!check_cwe)}
                        icon={<span style={{ fontSize: '16px', color: 'black' }}>✓</span>}
                        borderColor='black'
                    />
                    <Checkbox
                        name="cve"
                        label="cve"
                        checked={check_cve}
                        onChange={() => setCheckCVE(!check_cve)}
                        icon={<span style={{ fontSize: '16px', color: 'black' }}>✓</span>}
                        borderColor='black'
                    />
                    <Checkbox
                        name="products"
                        label="products"
                        checked={check_products}
                        onChange={() => setCheckProducts(!check_products)}
                        icon={<span style={{ fontSize: '16px', color: 'black' }}>✓</span>}
                        borderColor='black'
                    />
                    <Checkbox
                        name="exploits"
                        label="exploits"
                        checked={check_exploits}
                        onChange={() => setCheckExploits(!check_exploits)}
                        icon={<span style={{ fontSize: '16px', color: 'black' }}>✓</span>}
                        borderColor='black'
                    />
                    <Checkbox
                        name="installedsoftware"
                        label="installedsoftware"
                        checked={check_installedsoftware}
                        onChange={() => setCheckInstalledSoftware(!check_installedsoftware)}
                        icon={<span style={{ fontSize: '16px', color: 'black' }}>✓</span>}
                        borderColor='black'
                    />
                </div>
                <Button
                    onClick={() => {
                        setShowModal(true)
                    }}
                    className="btn-fill pull-right m-0"
                    type="submit"
                    variant="danger"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DELETE}
                </Button>
            </div>
            <Modal
                className="modal-mini modal-primary"
                show={showModal}
                onHide={() => setShowModal(false)}
            >
                {/* <Modal.Header className="justify-content-center">
                        <div className="modal-profile">
                            <i className="nc-icon nc-bulb-63"></i>
                        </div>
                    </Modal.Header> */}
                <Modal.Body className="text-center">
                    <p><b>{UI_MESSAGES.MODAL_DELETE_CONTENTS}</b><br />{UI_MESSAGES.BACKUP_NOTE}</p>
                </Modal.Body>
                <div className="modal-footer">
                    <Button
                        className="btn-simple"
                        type="button"
                        variant="danger"
                        onClick={async () => {

                            await fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/backupDB`)
                                .then(response => response.json())
                                .then(data => {
                                    if (data.backupFile) {
                                        const backupFileName = data.backupFileName;
                                        const backupFileContents = data.backupFile;

                                        // Create a temporary link element
                                        const link = document.createElement('a');
                                        link.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(backupFileContents);
                                        link.download = backupFileName;

                                        // Programmatically trigger the download
                                        link.click();
                                    } else {
                                        setErrorText(UI_MESSAGES.WARNING_BACKUP_NOT_FOUND);
                                    }
                                })
                                .catch(error => {
                                    setErrorText(UI_MESSAGES.ERROR_BACKUP, err);
                                });

                            deleteFromTables().forEach(query => {
                                executeQuery(query)
                            })
                            setShowModal(false)
                        }}
                    >
                        {UI_MESSAGES.BUTTON_DELETE}
                    </Button>
                    <Button
                        className="btn-simple"
                        type="button"
                        variant="info"
                        onClick={() => setShowModal(false)}
                    >
                        {UI_MESSAGES.BUTTON_CANCEL}
                    </Button>
                </div>
            </Modal>
        </Container>
    );
};

export default SettingsView;
