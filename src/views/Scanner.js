import React, { useEffect, useRef, useState } from "react";
import { Alert, Button, Container, Form } from "react-bootstrap";
import { scanInstalledSoftwareForCVEs } from "code/Scanner";
import CustomTable from "components/Table";
import { executeQuery } from "code/db";
import { getExploitsDB } from "code/db";
import { Label } from "reactstrap";
import { UI_MESSAGES } from "code/AppConstants";

const styles = {
    button: {
        marginBottom: 15
    },
    logText: {
        fontSize: 10,
        height: 100
    }
}

const ScannerView = props => {
    const logContainerRef = useRef(null);
    const [outputText, setOutputText] = useState('');
    const [disabled, setDisabled] = useState(false)
    const [tableData, setTableData] = useState([])
    const [exploitTable, setExploitTable] = useState([])
    const [successText, setSuccessText] = useState("")

    useEffect(() => {
        logContainerRef.current.scrollTop = logContainerRef.current.scrollHeight;
    }, [outputText]);

    // Function to match app names with exploits
    const matchExploitsWithApps = (exploits, appNames) => {
        const matchedExploits = [];


        for (const exploit of exploits) {
            for (const appName of appNames) {
                if (
                    Object.values(exploit).some((value) => {
                        if (typeof value === "string") {
                            return value.toLowerCase().includes(appName.toLowerCase());
                        }
                        return false;
                    })
                ) {
                    matchedExploits.push({ product: appName, exploit_file: exploit.file, ...((({ file, ...rest }) => rest)(exploit)) });
                }
                /* if (exploit.description.toLowerCase().includes(appName.toLowerCase())) {
                    matchedExploits.push({ product: appName, exploit_file: exploit.file, ...((({ file, ...rest }) => rest)(exploit)) });


                } */
            }
        }

        return matchedExploits;
    }

    // Function to run the script and set the output text state
    const runScript = async () => {
        setDisabled(true)
        setTableData([])
        setExploitTable([])
        setOutputText(UI_MESSAGES.SCANNER_INITIAL_MSG)
        const originalLog = console.log;
        console.log = (message) => {
            setOutputText((prevOutput) => prevOutput + message + '\n');
            originalLog(message);
        };

        // Run script
        const vuln = await scanInstalledSoftwareForCVEs();
        let exploits = await getExploitsDB().then(res => res.results)
        if (exploits.length == 0)
            console.log(UI_MESSAGES.TABLE_EXPLOIT_EMPTY)

        let result = vuln.map(elem => ({
            id: elem.affectedProduct.cve_id,
            displayName: elem.software.displayName,
            publisher: elem.software?.publisher,
            installed_version: elem.software?.displayVersion,
            vuln_version: elem.affectedProduct?.version,
            installDate: elem.software?.installDate
        }))

        const all_exploits = exploits
        setExploitTable(matchExploitsWithApps(exploits, vuln.map(elem => elem.software.displayName)))
        exploits = exploits.filter(exploit => result.some(cve => cve.id == exploit.cve_code))

        let queryString = 'SELECT * FROM cve WHERE id IN (';

        result.forEach((elem, index) => {
            queryString += `'${elem.id}'`;
            if (index !== result.length - 1) {
                queryString += ', ';
            }
        });

        queryString += ')';

        const cves = await executeQuery(queryString)

        result = result.map(elem => {
            const cve = cves.results.find(c => c.id === elem.id);
            const exploit = exploits.find(exploit => exploit.cve_code == cve.id)
            const cvss = cve.cvss_v3 ? cve.cvss_v3 : cve.cvss_v2
            let severity = 'N/A';
            if (cve) {
                if (cvss >= 9.0) {
                    severity = 'Critical';
                } else if (cvss >= 7.0) {
                    severity = 'High';
                } else if (cvss >= 4.0) {
                    severity = 'Medium';
                } else if (cvss >= 0.1) {
                    severity = 'Low';
                }
            }
            return {
                severity,
                id: elem.id,
                ProductName: elem.displayName,
                Vendor: elem.publisher,
                Vulnerable_version: elem?.vuln_version ?? 'N/A',
                Installed_version: elem?.installed_version ?? 'N/A',
                installDate: elem.installDate,
                cvss_v2: cve?.cvss_v2 ?? 'N/A',
                cvss_v3: cve?.cvss_v3 ?? 'N/A',
                exploit_id: exploit?.id,
                exploit_description: exploit?.description,
                exploit_file: exploit?.file
            };
        });

        setTableData(result.sort((a, b) => {
            // Treat 'N/A' as lowest value by using a small negative number (-1)
            const cvss_a = a.cvss_v3 === 'N/A' ? (a.cvss_v2 === 'N/A' ? -1 : a.cvss_v2) : a.cvss_v3;
            const cvss_b = b.cvss_v3 === 'N/A' ? (b.cvss_v2 === 'N/A' ? -1 : b.cvss_v2) : b.cvss_v3;
            return cvss_b - cvss_a;
        }));
        if (result.length == 0) {
            setSuccessText(UI_MESSAGES.ALERT_SUCCESS_NO_VULNERABILITIES)
        }
        // Restore original console.log function
        console.log = originalLog;
        setDisabled(false)
    };


    return (
        <Container fluid>
            <div className="output-box">
                <Button
                    onClick={runScript}
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    style={styles.button}
                    disabled={disabled}
                >
                    {UI_MESSAGES.BUTTON_SCAN}
                </Button>
                <Form.Group>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        value={outputText}
                        style={styles.logText}
                        ref={logContainerRef}
                    ></Form.Control>
                </Form.Group>
            </div>
            {
                tableData.length > 0 &&
                (<CustomTable tableData={tableData} cvelink={true} rowsToLoad={tableData.length} />)
            }
            {
                exploitTable.length > 0 &&
                (<div>
                    <h3 className="header">{UI_MESSAGES.HEADER_POTENTIAL_EXPLOITS}</h3>
                    <h5 className="header">{UI_MESSAGES.HEADER_EXPLOIT_DESCRIPTION}</h5>
                    <CustomTable tableData={exploitTable} rowsToLoad={exploitTable.length} />
                </div>)
            }
            {
                successText &&
                <Alert variant="success" className="mt-5">
                    <button
                        aria-hidden={true}
                        className="close"
                        data-dismiss="alert"
                        type="button"
                        onClick={() => { setSuccessText("") }}
                    >
                        <i className="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                        {successText}
                    </span>
                </Alert>
            }
        </Container>
    )

}

export default ScannerView