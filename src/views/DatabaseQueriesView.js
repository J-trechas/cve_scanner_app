import React, {  useState } from "react";
import { Alert, Button, Card, Container, Dropdown, Form, FormLabel, Table } from "react-bootstrap";
import { executeQuery } from "code/db";
import { queryList } from "code/AppConstants";
import CustomTable from "components/Table";
import { RingLoader } from "react-spinners";
import { UI_MESSAGES } from "code/AppConstants";


const styles = {
    button: {
        marginTop: 15,
        marginBottom: 15
    },
    logText: {
        fontSize: 14,
        height: 200
    },
    dropdown: {
        border: '2px solid #1DC7EA',
        outline: 'none',
        color: 'white',
        backgroundColor: "#1DC7EA",
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 15,
    }
}



const DatabaseQueriesView = props => {
    const [tableData, setTableData] = useState([]);
    const [errorText, setErrorText] = useState("");
    const [query, setQuery] = useState("");
    const [displayedRows, setDisplayedRows] = useState(10);
    const [isLoading, setIsLoading] = useState(false);

    const runquery = async () => {
        setTableData([])
        setIsLoading(true)
        try {
            setErrorText("")
            await executeQuery(query)
                .then((data) => {
                    if (data) {
                        if (data.results.length === 0) {
                            setErrorText(UI_MESSAGES.ALERT_NO_RESULTS);
                        } else {
                            setTableData([...data.results]);
                        }
                    }
                })
                .catch((error) => {
                    setErrorText(UI_MESSAGES.ALERT_ERROR_EXECUTING_QUERY, error);
                });
        } catch (error) {
            setErrorText(UI_MESSAGES.ALERT_ERROR_EXECUTING_QUERY, error);
        }
        setIsLoading(false)
    };

    const handleQuerySelect = (e, query) => {
        e.preventDefault();
        setQuery(query);
    };


    return (
        <Container fluid>
            <div className="output-box">
                <Form.Group>
                    <FormLabel>{UI_MESSAGES.QUERY_BOX_LABEL}</FormLabel>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        style={styles.logText}
                        value={query}
                        onChange={(e) => setQuery(e.target.value)}
                    ></Form.Control>
                </Form.Group>
                <Dropdown /* as={Nav.Item} */>
                    <Dropdown.Toggle
                        style={styles.dropdown}
                        aria-expanded={false}
                        aria-haspopup={true}
                        /* as={Nav.Link} */
                        data-toggle="dropdown"
                        id="navbarDropdownMenuLink"
                        variant="default"
                        className="m-0 mt-2"
                    >
                        <span className="no-icon">{UI_MESSAGES.EXAMPLE_QUERY_LIST}</span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu aria-labelledby="navbarDropdownMenuLink">
                        {queryList.map((query, index) =>
                            <Dropdown.Item
                                key={index}
                                onClick={(e) => handleQuerySelect(e, query.sql)}
                                style={{ fontSize: 12 }}
                            >
                                {query.description}
                            </Dropdown.Item>
                        )}
                    </Dropdown.Menu>
                </Dropdown>
            </div>
            <Button
                onClick={runquery}
                className="btn-fill pull-right"
                type="submit"
                variant="info"
                style={styles.button}
            >
                {UI_MESSAGES.BUTTON_RUN}
            </Button>
            {
                errorText &&
                <Alert variant="danger">
                    <button
                        aria-hidden={true}
                        className="close"
                        data-dismiss="alert"
                        type="button"
                        onClick={() => { setErrorText("") }}
                    >
                        <i className="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                        {errorText}
                    </span>
                </Alert>
            }
            {
                isLoading &&
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <RingLoader color="#1DC7EA" size={50} />
                </div>
            }
            {
                tableData.length > 0 &&
                (
                    <div>
                        <Alert variant="success" style={{ width: 'fit-content', marginTop: 10, borderRadius: '5px', padding: '0.75rem 1.25rem' }}>
                            <span>
                                {UI_MESSAGES.ALERT_QUERY_SUCCESS.replace('{number}', tableData.length)}
                            </span>
                        </Alert>
                        <CustomTable tableData={tableData} rowsToLoad={30} />
                    </div>
                )
            }
        </Container >
    )

}

export default DatabaseQueriesView