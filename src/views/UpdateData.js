import React, { useEffect, useRef, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { updateCVEs } from "code/db";
import { getCveDB } from "code/db";
import CustomTable from "components/Table";
import { UI_MESSAGES } from "code/AppConstants";

const styles = {
    button: {
        marginBottom: 10,
        width: 'fit-content'
    },
    logText: {
        fontSize: 10,
        height: 200
    }
}

const UpdateDataView = props => {
    const logContainerRef = useRef(null);
    const [outputText, setOutputText] = useState('');
    const [disabled, setDisabled] = useState(false)
    const [tableData, setTableData] = useState([]);

    useEffect(() => {
        logContainerRef.current.scrollTop = logContainerRef.current.scrollHeight;
    }, [outputText]);

    // Function to run the script and set the output text state
    const runScript = async () => {
        setDisabled(true)
        setOutputText(UI_MESSAGES.UPDATE_INITIAL_MSG)
        const originalLog = console.log;
        console.log = (message) => {
            setOutputText((prevOutput) => prevOutput + message + '\n');
            originalLog(message);
        };



        // Run script
        const cves = await getCveDB();
        const data = await updateCVEs();

        let out = ""
        data.data.forEach(cve => {
            out += UI_MESSAGES.UPDATE_ADDED_MODIFIED_MSG.replace('{cve}', cve.id).replace('{action}', cves.results.map(cve => cve.id).includes(cve.id) ? UI_MESSAGES.UPDATE_MODIFIED : UI_MESSAGES.UPDATE_ADDED)
        });
        /* out += `Total: ${data.data.length} CVE${data.data.length > 1 ? 's' : ''}.\n`; */
        out += UI_MESSAGES.UPDATE_WARNING
        setOutputText(out)
        setDisabled(false)
        setTableData(data.data)
        // Restore original console.log function
        console.log = originalLog;
    };


    return (
        <Container fluid>
            <div className="output-box">
                <div style={{ display: 'flex', flexDirection: 'column' }}>

                    <Button
                        onClick={runScript}
                        className="btn-fill pull-right"
                        type="submit"
                        variant="info"
                        style={styles.button}
                        disabled={disabled}
                    >
                        {UI_MESSAGES.BUTTON_UPDATE}
                    </Button>
                    <span style={{ backgroundColor: '#f8f9fa', borderRadius: '5px', color: '#6c757d', fontSize: 12 }}>{UI_MESSAGES.INFO_CVE_ADDITIONS}<a href={UI_MESSAGES.LINK_RECENT_FEED_HIGHLIGHT} target="_blank">{UI_MESSAGES.LINK_RECENT_FEED}</a></span>
                    <span style={{ backgroundColor: '#f8f9fa', marginBottom: 5, borderRadius: '5px', color: '#6c757d', fontSize: 12 }}>{UI_MESSAGES.INFO_CVE_MODIFIED}<a href={UI_MESSAGES.LINK_RECENT_FEED_HIGHLIGHT} target="_blank">{UI_MESSAGES.LINK_MODIFIED_FEED}</a></span>
                </div>
                <Form.Group>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        value={outputText}
                        style={styles.logText}
                        ref={logContainerRef}
                    ></Form.Control>
                </Form.Group>
            </div>
            {
                tableData.length > 0 &&
                (<CustomTable tableData={tableData} cvelink={true} rowsToLoad={tableData.length} />)
            }
        </Container>
    )

}

export default UpdateDataView