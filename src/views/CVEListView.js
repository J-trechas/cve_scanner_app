import React, { useEffect, useState } from "react";
import { Alert, Container, Form } from "react-bootstrap";
import CustomTable from "components/Table";
import { getCveDB } from "code/db";
import { getExploitsDB } from "code/db";
import { RingLoader } from "react-spinners";
import { UI_MESSAGES } from "code/AppConstants";


const styles = {
    searchInput: {
        width: 300,
        margin: "0 10px",
    }
}

const CVEListView = props => {
    const [tableData, setTableData] = useState([]);
    const [filteredTableData, setFilteredTableData] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchData() {
            const cves = await getCveDB();
            let exploits = await getExploitsDB().then(res => res.results)
            exploits = exploits.filter(exploit => cves.results.some(cve => cve.id == exploit.cve_code))
            let result = cves.results.map(cve => {
                const exploit = exploits.find(exploit => exploit.cve_code == cve.id)

                return {
                    ...cve,
                    exploit_id: exploit?.id ?? null,
                    exploit_description: exploit?.description ?? null,
                    exploit_file: exploit?.file ?? null

                }
            })

            setTableData(result);
            setFilteredTableData(result);
            setLoading(false)
        }
        fetchData();
    }, []);

    const handleSearch = (event) => {
        const query = event.target.value.toLowerCase();
        const filteredData = tableData.filter((row) =>
            row.id.toLowerCase().includes(query) || row.summary.toLowerCase().includes(query)
        );
        setFilteredTableData(filteredData);
    };

    return (
        <Container fluid>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <Form>
                    <Form.Group controlId="search">
                        <Form.Control
                            style={styles.searchInput}
                            type="text"
                            placeholder={UI_MESSAGES.CVE_SEARCH_PLACEHOLDER}
                            onChange={handleSearch}
                        />
                    </Form.Group>
                </Form>
                <Alert variant="success" style={{ width: 'fit-content', /* marginTop: 0, marginBottom: 15, marginLeft: 10, */ borderRadius: '5px', padding: '0.75rem 1.25rem' }}>
                    <span>
                        {UI_MESSAGES.TOTAL_CVES_DISPLAYED}: {filteredTableData.length} ({UI_MESSAGES.CVE_LIST_OWASP_NOTE})<br />
                        {UI_MESSAGES.TOTAL_EXPLOITS_DISPLAYED}: {filteredTableData.filter(cve => cve.exploit_id != null).length}
                    </span>
                </Alert>
            </div>
            {loading ? (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '400px' }}>
                    <RingLoader color="#1DC7EA" size={100} />
                </div>
            ) : filteredTableData.length > 0 ? (
                <div>
                    <CustomTable tableData={filteredTableData} rowsToLoad={1000} cvelink={true} />
                </div>
            ) : (
                <div>{UI_MESSAGES.NO_DATA_AVAILABLE}</div>
            )}
        </Container>
    )

}

export default CVEListView
