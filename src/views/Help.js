import { UI_MESSAGES } from 'code/AppConstants';
import React, { useState, useEffect } from 'react';
import { Alert, Button, Container, Form } from 'react-bootstrap';
import { Label } from 'reactstrap';

const styles = {
    button: {
        marginBottom: 15
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'start',
        marginBottom: 20
    },
    label: {
        marginBottom: 0,
        marginRight: '10px',
        fontWeight: 'bold'
    }
}

const HelpView = () => {
    const [guideHtml, setGuideHtml] = useState("")
    const [errorText, setErrorText] = useState("")

    return (
        <Container fluid>
            {
                errorText &&
                <Alert variant="danger">
                    <button
                        aria-hidden={true}
                        className="close"
                        data-dismiss="alert"
                        type="button"
                        onClick={() => { setErrorText("") }}
                    >
                        <i className="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                        {errorText}
                    </span>
                </Alert>
            }
            <div className="output-box" style={styles.container}>
                <Label style={styles.label}>{UI_MESSAGES.LABEL_USER_GUIDE}</Label>
                <Button
                    onClick={() => {
                        window.open(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/guide`);
                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DOWNLOAD}
                </Button>
                <Button
                    onClick={() => {


                        setErrorText("")

                        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/guide_raw`)
                            .then(response => {
                                if (response.ok) {
                                    return response.text();
                                } else {
                                    throw new Error(`Server responded with status: ${response.status}`);
                                }
                            })
                            .then(data => {
                                setGuideHtml(data);
                                console.log(data)
                            })
                            .catch(error => {
                                setErrorText(UI_MESSAGES.ERROR_FETCH, error);
                            });

                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DISPLAY}
                </Button>
            </div>
            <div className="output-box" style={styles.container}>
                <Label style={styles.label}>{UI_MESSAGES.LABEL_DEVELOPER_GUIDE}</Label>
                <Button
                    onClick={() => {
                        window.open(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/guide?use=dev`);
                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DOWNLOAD}
                </Button>
                <Button
                    onClick={() => {

                        setErrorText("")

                        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:5000'}/guide_raw?use=dev`)
                            .then(response => {
                                if (response.ok) {
                                    return response.text();
                                } else {
                                    throw new Error(`Server responded with status: ${response.status}`);
                                }
                            })
                            .then(data => {
                                setGuideHtml(data);
                                console.log(data)
                            })
                            .catch(error => {
                                setErrorText(UI_MESSAGES.ERROR_FETCH, error);
                            });

                    }}
                    className="btn-fill pull-right m-0 ml-2"
                    type="submit"
                    variant="info"
                    style={styles.button}
                >
                    {UI_MESSAGES.BUTTON_DISPLAY}
                </Button>
            </div>
            {guideHtml &&
                <div dangerouslySetInnerHTML={{ __html: guideHtml }} />
            }
        </Container>
    );
};

export default HelpView;
