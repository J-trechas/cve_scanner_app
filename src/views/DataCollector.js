import React, { useEffect, useRef, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { allDataCollector } from "code/DataCollector";
import { UI_MESSAGES } from "code/AppConstants";
import { collectAllCVEsNVD } from "code/CVE";

const styles = {
    button: {
        marginBottom: 15,
        marginRight: 15
    },
    logText: {
        fontSize: 10,
        height: 350
    }
}

const DataCollectorView = props => {
    const logContainerRef = useRef(null);
    const [outputText, setOutputText] = useState('');
    const [disabled, setDisabled] = useState(false)

    useEffect(() => {
        logContainerRef.current.scrollTop = logContainerRef.current.scrollHeight;
    }, [outputText]);

    // Function to run the script and set the output text state
    const dataCollect = async (onlyCWEs) => {
        setOutputText("")
        setDisabled(true)
        const originalLog = console.log;
        console.log = (message) => {
            setOutputText((prevOutput) => prevOutput + message + '\n');
            originalLog(message);
        };
        // Run script
        await allDataCollector(onlyCWEs);

        // Restore original console.log function
        console.log = originalLog;
        setDisabled(false)
    };
    const allDataCollect = async () => {
        setOutputText("")
        setDisabled(true)
        const originalLog = console.log;
        console.log = (message) => {
            setOutputText((prevOutput) => prevOutput + message + '\n');
            originalLog(message);
        };
        // Run script
        await collectAllCVEsNVD();

        // Restore original console.log function
        console.log = originalLog;
        setDisabled(false)
    };


    return (
        <Container fluid>
            <div className="output-box">
                <Button
                    onClick={() => dataCollect(true)}
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    style={styles.button}
                    disabled={disabled}
                >
                    {UI_MESSAGES.BUTTON_COLLECT_CWES}
                </Button>
                <Button
                    onClick={() => dataCollect(false)}
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    style={styles.button}
                    disabled={disabled}
                >
                    {UI_MESSAGES.BUTTON_COLLECT_CVES_PRODUCTS}
                </Button>
                <Button
                    onClick={() => allDataCollect(false)}
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    style={styles.button}
                    disabled={disabled}
                >
                    {UI_MESSAGES.BUTTON_COLLECT_ALL_CVES_PRODUCTS}
                </Button>
                <Form.Group>
                    <Form.Control
                        cols="80"
                        placeholder=""
                        rows="4"
                        as="textarea"
                        value={outputText}
                        style={styles.logText}
                        ref={logContainerRef}
                    ></Form.Control>
                </Form.Group>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <span style={{ backgroundColor: '#f8f9fa', paddingTop: '10px', borderRadius: '5px', color: '#6c757d', fontSize: 12 }}>{UI_MESSAGES.LINK_CWES_DESCRIPTION}<a href={UI_MESSAGES.LINK_CWES_SOURCE} target="_blank">{UI_MESSAGES.LINK_CWES_SOURCE}</a></span>
                    <span style={{ backgroundColor: '#f8f9fa', paddingTop: '10px', borderRadius: '5px', color: '#6c757d', fontSize: 12 }}>{UI_MESSAGES.LINK_CVES_DESCRIPTION}<a href={UI_MESSAGES.LINK_CVES_SOURCE_HIGHLIGHT} target="_blank">{UI_MESSAGES.LINK_CVES_SOURCE}</a></span>
                </div>

            </div>
        </Container>

    )

}

export default DataCollectorView