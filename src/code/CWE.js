import { OWASP_TOP10_CWE, UI_MESSAGES } from "./AppConstants";
import { insertQuery } from "./db"

const collectCWEs = async () => {

    let html = ""
    try {
        // Get Page with the OWASP CWEs
        html = await fetch(OWASP_TOP10_CWE)
            .then((res) => res.text());

    } catch (error) {
        // Handle errors here
        if (error instanceof TypeError) {
            // Connection error
            console.log(UI_MESSAGES.CONNECTION_ERROR);
            console.log(error.message);

        } else {
            // Page is down or other fetch-related error
            console.log(UI_MESSAGES.SERVER_UNAVAILABLE);
            console.log(error.message);

        }
        return []
    }


    let listEnabled = false
    let count = 0
    let cweDict = []
    html = html.split('\n')

    // Parse the list of mapped CWEs
    html.forEach(line => {
        if (line.match("https://cwe.mitre.org/data/definitions/")) {
            listEnabled = true
        }

        let cwe = line.match(/CWE-([0-9]{3})\s(.+)\<\/a\>/)
        if (cwe && listEnabled) {
            count += 1
            cweDict = [...cweDict, {
                id: cwe[1],
                Summary: cwe[2],
                total_cve: 0,
            }]
        }
    });

    //Format list to insert into DB
    let clone = JSON.parse(JSON.stringify(cweDict))
    let cwelist = clone.map(cwe => {
        return Object.values(cwe)
    })

    //Insert CWEs into DB
    let stmt = `INSERT INTO cwe VALUES ? ON DUPLICATE KEY UPDATE id=VALUES(id),summary=VALUES(summary)`;
    insertQuery(stmt, cwelist)

    return cweDict
}

export default collectCWEs;

