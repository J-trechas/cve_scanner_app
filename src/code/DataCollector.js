import collectCWEs from "./CWE";
import { collectCVEsNVD } from "./CVE";
import { executeQuery, getCweDB, insertQuery } from "./db";
import { insertProductsDB } from "./Products";
import { UI_MESSAGES } from "./AppConstants";



export const allDataCollector = async (onlyCWEs = false) => {
    var cweDict = []

    if (onlyCWEs) {
        // Collect from web
        cweDict = await collectCWEs()
        if (cweDict.length > 0) {
            cweDict.forEach(cwe => console.log("CWE-" + cwe.id + ": " + cwe.Summary))
            console.log(UI_MESSAGES.TOTAL_CWES_COLLECTED + cweDict.length)
        }

        return
    } else {
        // Get from DB
        const results = await getCweDB()
        cweDict = [...results.results]
        if (cweDict.length == 0) {
            console.log(UI_MESSAGES.CWE_TABLE_EMPTY)
            return
        }
    }

    // add list of CVEs for every CWE
    cweDict = await collectCVEsNVD(cweDict)
    if (!cweDict) return

    console.log(UI_MESSAGES.WAIT_TO_IMPORT)

    // Get all info on every cve and insert into DB
    for (let index in cweDict) {

        let cwe = cweDict[index]

        /* if (!CWE_ALLOWED_LIST.includes(Number(cwe.id))) continue */ //remove

        let cvelist = []

        for (let index in cwe.CVEs) {
            try {
                let cve = cwe.CVEs[index];

                //Insert all products affected into DB
                await insertProductsDB(cve.vendors, cve.id)

                let products_affected = 0;
                for (let index in cve.vendors) {
                    let vendor = cve.vendors[index];
                    products_affected += vendor.length
                }

                cvelist = [...cvelist, [cve.id, cve.summary, cwe.id, products_affected, cve.cvssV2, cve.cvssV3, cve.publishDate]]
            } catch (error) {
                console.log(error)
            }
        }

        //Insert CVEs into DB
        if (cvelist.length > 0) {
            let stmt = `INSERT INTO cve VALUES ? ON DUPLICATE KEY UPDATE id=VALUES(id),summary=VALUES(summary),cwe_id=VALUES(cwe_id),products_affected=VALUES(products_affected),cvss_v2=VALUES(cvss_v2),cvss_v3=VALUES(cvss_v3),date_published=VALUES(date_published)`;
            await insertQuery(stmt, cvelist)/* .then(res => console.log('Rows inserted in cve:' + res.results.affectedRows)) */
        }
    }
    let query = `SELECT SUM(total_cve) AS total_cve_count FROM cwe`;
    let total = await executeQuery(query);
    console.log(UI_MESSAGES.TOTAL_CVES_IMPORTED.replace('{number}', total?.results?.[0].total_cve_count))

}