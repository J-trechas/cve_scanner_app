
export const executeQuery = async (query) => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}/execute-query`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query }),
        });

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error executing the query:', error);
        throw new Error(error)
    }
}

export const insertQuery = async (query, values = []) => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}/insert-query`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query, values }),
        });

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error executing the query:', query);
        console.log(error);
        return null;
    }
};


const fetchTableData = async (endpoint) => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}${endpoint}`);

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error fetching data from server');
        return null;
    }
};

export const getCweDB = async () => {
    return await fetchTableData('/cwe');
};

export const getCveDB = async () => {
    return await fetchTableData('/cve');
};

export const getExploitsDB = async () => {
    return await fetchTableData('/exploits');
};

export const getInstalledSoftwareDB = async () => {
    return await fetchTableData('/installedsoftware');
};

export const getProductsDB = async () => {
    return await fetchTableData('/products');
};

export const getLocalInstalledSoftware = async (store = false) => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';
    const url = store ? `${apiUrl}/local-installed-software?store=true` : `${apiUrl}/local-installed-software`;

    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error fetching local installed software:', error);
        return null;
    }
};

export const collectExploits = async () => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}/exploits-data`);
        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error fetching exploits data:', error);
        return null;
    }
};

export const updateCVEs = async () => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}/update-cve`);
        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error updating CVEs: ', error);
        return null;
    }
};

export const fetchRequest = async (url, apiKey) => {
    const apiUrl = process.env.REACT_APP_API_URL || 'http://localhost:5000';

    try {
        const response = await fetch(`${apiUrl}/fetch`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ url, apiKey }),
        });

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        // Handle errors here
        if (error instanceof TypeError) {
            // Connection error
            console.log('Connection error. Please check your internet connection.');
            console.log(error.message);

        } else {
            // Page is down or other fetch-related error
            console.log('The page is down or the server is unavailable. Please try again later.');
            console.log(error.message);

        }

        return null;
    }
};



