import levenshtein from 'fast-levenshtein'
import { getInstalledSoftwareDB, getProductsDB } from './db';
import { UI_MESSAGES } from './AppConstants';


function findMatchingProduct(displayName, affectedProducts, threshold = 0.8) {
    let bestMatch = [];
    let bestMatchDistance = 0;
    for (const product of affectedProducts) {
        const distance = levenshtein.get(displayName, product.product);
        const similarity = 1 - distance / Math.max(displayName.length, product.product.length);

        if (similarity > threshold && similarity >= bestMatchDistance) {
            if (similarity > bestMatchDistance) {
                bestMatch = [product];
                bestMatchDistance = similarity;
            } else {
                bestMatch.push(product);
            }
        }
    }

    return bestMatch;
}


function checkVersion(installedSoftware, product) {
    const installedVersion = installedSoftware.displayVersion?.trim();
    const versionRange = product.version?.split(' up_to ');

    if (!installedVersion || !versionRange) return true

    if (versionRange.length === 2) {
        const versionLow = versionRange[0].trim();
        const versionHigh = versionRange[1].trim();

        if (compareVersions(installedVersion, versionLow) >= 0 && compareVersions(installedVersion, versionHigh) <= 0) {
            return true;
        }
    } else if (installedVersion == versionRange) {
        return true;
    }

    return false;
}

function compareVersions(version1, version2) {
    const parts1 = version1.split('.');
    const parts2 = version2.split('.');

    for (let i = 0; i < Math.max(parts1.length, parts2.length); i++) {
        const part1 = parts1[i] || '';
        const part2 = parts2[i] || '';

        const comparison = part1.localeCompare(part2, undefined, { numeric: true, sensitivity: 'base' });

        if (comparison < 0) {
            return -1;
        } else if (comparison > 0) {
            return 1;
        }
    }

    return 0;
}




function scanForMatch(installedSoftware, products) {
    const vulnerableSoftware = [];
    for (const software of installedSoftware) {

        if (!software?.displayName || software.active == 0) continue

        const matchingProduct = findMatchingProduct(software.displayName.toLowerCase(), products);

        if (matchingProduct.length > 0) {
            matchingProduct.forEach(product => {

                if (checkVersion(software, product)) {
                    vulnerableSoftware.push({
                        software,
                        affectedProduct: { ...product },
                    });
                }
            })
        }
    }

    return vulnerableSoftware;
}




export async function scanInstalledSoftwareForCVEs() {

    let installedSoftware = await getInstalledSoftwareDB();
    installedSoftware = installedSoftware.results
    if (installedSoftware.length > 0)
        console.log(UI_MESSAGES.SCANNING_SOFTWARE_AMOUNT.replace('{total}', installedSoftware.length));
    else {
        console.log(UI_MESSAGES.WARNING_INSTALLED_SOFTWARE_EMPTY)
    }

    let products = await getProductsDB()
    products = products.results
    if (products.length == 0)
        console.log(UI_MESSAGES.WARNING_PRODUCTS_EMPTY)


    const vulnerabilities = scanForMatch(installedSoftware, products);

    console.log(UI_MESSAGES.VULNERABILITIES_FOUND.replace('{total}', vulnerabilities.length));
    console.log(UI_MESSAGES.SEVERITY_DESCRIPTION);

    return vulnerabilities

}

