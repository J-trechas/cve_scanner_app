import { NVD_API_KEY, NVD_API_URL, UI_MESSAGES } from "./AppConstants"
import { insertProductsDB } from "./Products";
import { sleep } from "./Utils"
import { fetchRequest, insertQuery } from "./db"


export const collectCVEsNVD = async (cweIds) => {
    const PAGE_SIZE = 100;

    for (let cweId in cweIds) {
        const index = cweId
        cweId = cweIds[cweId]
        /* if (!CWE_ALLOWED_LIST.includes(Number(cweId.id))) continue */ //remove

        console.log(UI_MESSAGES.COLLECTING_CVES_FOR_CWE.replace('{cwe}', cweId.id));

        let cveArray = [];
        let page = 1;
        let totalPages = null;
        do {
            const url = `${NVD_API_URL}?resultsPerPage=${PAGE_SIZE}&startIndex=${(page - 1) * PAGE_SIZE}&cweId=CWE-${cweId.id}`;
            /* const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'apiKey': NVD_API_KEY
                }
            }); */
            let res = await fetchRequest(url, NVD_API_KEY)

            if (!res) return null

            if (res.status != 200) {
                if (res.status == 429) {
                    console.log(UI_MESSAGES.ERROR_429)
                    console.log(UI_MESSAGES.PAUSE_FOR_10_MINUTES)
                    const endTime = new Date(Date.now() + (10 * 60 * 1000));
                    console.log(UI_MESSAGES.RESUME_AT.replace('{time}', `${endTime.getHours().toString().padStart(2, '0')}:${endTime.getMinutes().toString().padStart(2, '0')}`));
                    await sleep(10 * 60 * 1000);

                }
            }

            const data = res.data;

            if (totalPages === null) {
                totalPages = Math.ceil(data.totalResults / PAGE_SIZE);
                console.log(UI_MESSAGES.TOTAL_PAGES_FOR_CWE.replace('{cwe}', cweId.id).replace('{totalPages}', totalPages));
            }
            if (data.vulnerabilities) {

                cveArray = cveArray.concat(data.vulnerabilities.map(item => {

                    const vendors = {};
                    const configs = item?.cve?.configurations ?? null;
                    if (configs && configs.length > 0) {

                        configs.forEach(config => {
                            let nodes = config.nodes
                            if (nodes && nodes.length > 0) {

                                nodes.forEach(node => {
                                    if (node.cpeMatch && node.cpeMatch.length > 0) {
                                        const cpes = node.cpeMatch;

                                        cpes.forEach(cpe => {
                                            let cpeMatch = cpe.criteria.split(':')
                                            const vendor = cpeMatch[3];
                                            const product = cpeMatch[4];
                                            let version = cpeMatch.length > 5 ? cpeMatch[5] : null;
                                            if (cpe.versionStartIncluding || cpe.versionStartExcluding || cpe.versionEndExcluding || cpe.versionEndIncluding) {
                                                const startVersion = cpe?.versionStartExcluding ?? cpe?.versionStartIncluding ?? "0";
                                                const endVersion = cpe?.versionEndExcluding ?? cpe?.versionEndIncluding ?? "now";
                                                version = `${startVersion} up_to ${endVersion}`;
                                            }

                                            if (vendor in vendors) {
                                                vendors[vendor].push({ product, version });
                                            } else {
                                                vendors[vendor] = [{ product, version }];
                                            }
                                        })
                                    }
                                })
                            }
                        }
                        )
                    }

                    return {

                        id: item.cve.id,
                        summary: item.cve.descriptions[0].value,
                        cweId: cweId.id,
                        cvssV2: item.cve.metrics.cvssMetricV2?.[0]?.cvssData?.baseScore ?? null,
                        cvssV3: item.cve.metrics.cvssMetricV3?.[0]?.cvssData?.baseScore || item.cve.metrics.cvssMetricV31?.[0]?.cvssData?.baseScore || null,
                        publishDate: item.cve.published,
                        vendors: vendors
                    };
                }));
            }

            console.log(UI_MESSAGES.COLLECTED_CVES_FOR_CWE.replace('{cwe}', cweId.id).replace('{total}', cveArray.length));
            if (page >= totalPages) {
                console.log(UI_MESSAGES.DONE_WITH_CWE.replace('{cwe}', cweId.id));
            }

            page++;
        } while (page <= totalPages);

        cweIds[index] = {
            ...cweIds[index],
            total_cve: cveArray.length,
            CVEs: cveArray
        }
    }

    return cweIds;
}

export const collectAllCVEsNVD = async () => {

    console.log(UI_MESSAGES.COLLECTING_CVES)

    const resultsPerPage = 2000; // Number of results per page
    let startIndex = 0; // Starting index

    try {
        while (true) {

            let cveArray = []

            // Send a request to fetch CVEs for the current page
            const url = `${NVD_API_URL}?resultsPerPage=${resultsPerPage}&startIndex=${startIndex}`;

            let res = await fetchRequest(url, NVD_API_KEY)

            if (!res) return

            if (res.status != 200) {
                if (res.status == 429) {
                    console.log(UI_MESSAGES.ERROR_429)
                    console.log(UI_MESSAGES.PAUSE_FOR_10_MINUTES)
                    const endTime = new Date(Date.now() + (10 * 60 * 1000));
                    console.log(UI_MESSAGES.RESUME_AT.replace('{time}', `${endTime.getHours().toString().padStart(2, '0')}:${endTime.getMinutes().toString().padStart(2, '0')}`));
                    await sleep(10 * 60 * 1000);

                }
            }

            const data = res.data;

            if (data.vulnerabilities) {

                cveArray = data.vulnerabilities.map(item => {

                    const vendors = {};
                    const configs = item?.cve?.configurations ?? null;
                    if (configs && configs.length > 0) {

                        configs.forEach(config => {
                            let nodes = config.nodes
                            if (nodes && nodes.length > 0) {

                                nodes.forEach(node => {
                                    if (node.cpeMatch && node.cpeMatch.length > 0) {
                                        const cpes = node.cpeMatch;

                                        cpes.forEach(cpe => {
                                            let cpeMatch = cpe.criteria.split(':')
                                            const vendor = cpeMatch[3];
                                            const product = cpeMatch[4];
                                            let version = cpeMatch.length > 5 ? cpeMatch[5] : null;
                                            if (cpe.versionStartIncluding || cpe.versionStartExcluding || cpe.versionEndExcluding || cpe.versionEndIncluding) {
                                                const startVersion = cpe?.versionStartExcluding ?? cpe?.versionStartIncluding ?? "0";
                                                const endVersion = cpe?.versionEndExcluding ?? cpe?.versionEndIncluding ?? "now";
                                                version = `${startVersion} up_to ${endVersion}`;
                                            }

                                            if (vendor in vendors) {
                                                vendors[vendor].push({ product, version });
                                            } else {
                                                vendors[vendor] = [{ product, version }];
                                            }
                                        })
                                    }
                                })
                            }
                        }
                        )
                    }

                    const cweField = item.cve.weaknesses?.[0]?.description?.[0]?.value;
                    const cweNumber = (cweField && cweField.match(/-(\d+)$/)?.[1] || -1) - 0;


                    return {

                        id: item.cve.id,
                        summary: item.cve.descriptions[0].value,
                        cweId: cweNumber,
                        cvssV2: item.cve.metrics.cvssMetricV2?.[0]?.cvssData?.baseScore ?? null,
                        cvssV3: item.cve.metrics.cvssMetricV3?.[0]?.cvssData?.baseScore || item.cve.metrics.cvssMetricV31?.[0]?.cvssData?.baseScore || null,
                        publishDate: item.cve.published,
                        vendors: vendors
                    };
                });
            }

            //Insert CVEs into DB
            if (cveArray.length > 0) {

                let cvelist = []

                for (let index in cveArray) {
                    try {
                        let cve = cveArray[index];

                        //Insert all products affected into DB
                        await insertProductsDB(cve.vendors, cve.id)

                        let products_affected = 0;
                        for (let index in cve.vendors) {
                            let vendor = cve.vendors[index];
                            products_affected += vendor.length
                        }

                        cvelist = [...cvelist, [cve.id, cve.summary, cve.cweId, products_affected, cve.cvssV2, cve.cvssV3, cve.publishDate]]
                    } catch (error) {
                        console.log(error)
                    }
                }

                let stmt = `INSERT INTO cve VALUES ? ON DUPLICATE KEY UPDATE id=VALUES(id),summary=VALUES(summary),cwe_id=VALUES(cwe_id),products_affected=VALUES(products_affected),cvss_v2=VALUES(cvss_v2),cvss_v3=VALUES(cvss_v3),date_published=VALUES(date_published)`;
                await insertQuery(stmt, cvelist)
            }

            console.log(UI_MESSAGES.COLLECTED_OUT_OF.replace('{number}', startIndex + data.vulnerabilities.length).replace('{total}', data.totalResults))

            // Increment the start index for the next page
            startIndex += resultsPerPage;

            // Check if there are more CVEs to fetch
            if (startIndex >= data.totalResults) {
                break;
            }


        }
    } catch (error) {
        console.error('Error:', error);
    }
}
