export const NVD_API_KEY = "19adfd81-bd75-4230-8600-3be61b474351"
export const NVD_API_URL = 'https://services.nvd.nist.gov/rest/json/cves/2.0'
export const OWASP_TOP10_CWE = 'https://owasp.org/Top10/A02_2021-Cryptographic_Failures/'

/* 
export const CWE_ALLOWED_LIST = [261, 296, 310, 319, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 335, 336, 337, 338, 340, 347, 523, 720, 757, 759, 760, 780, 818, 916]
 */

export const queryList = [
    {
        description: "Return all CVEs based on a CWE id",
        sql: "SELECT * FROM cve WHERE cwe_id='335'"
    },
    {
        description: "Return all CVEs with cvss_v3 > 7",
        sql: "SELECT * FROM cve WHERE cvss_v3 > 7"
    },
    {
        description: "Return all CVEs with 0 CPEs",
        sql: "SELECT * FROM cve WHERE products_affected = 0"
    },
    {
        description: "Return all CVEs published in a particular time range",
        sql: "SELECT * FROM cve WHERE date_published>'2022-01-01' and date_published < '2022-12-31'"
    },
    {
        description: "Return all CVEs who are related to Mozilla (Vendor)",
        sql: "SELECT * FROM cve INNER JOIN products ON cve.id=products.cve_id WHERE products.vendor = 'mozilla'"
    },
    {
        description: "Return all CVEs who are related to Thunderbird (Product)",
        sql: "SELECT * FROM cve INNER JOIN products ON cve.id=products.cve_id WHERE products.product = 'thunderbird'"
    },
    {
        description: "Return all products for a particular CVE",
        sql: "SELECT * FROM products WHERE cve_id='CVE-2015-3405'"
    },
    {
        description: "Return all software installed in a particular time range",
        sql: "SELECT * FROM installedsoftware WHERE installDate>'2022-01-01' and installDate<'2022-12-31'"
    },
    {
        description: "Return all exploits for a particular product",
        sql: "SELECT * FROM exploits WHERE description LIKE '%thunderbird%'"
    },
    {
        description: "Return all affected software that is installed on the device that have an exploit",
        sql: "SELECT installedsoftware.displayName,installedsoftware.displayVersion,exploits.id,exploits.description,exploits.cve_code,exploits.file FROM exploits,installedsoftware WHERE exploits.description like CONCAT('%', installedsoftware.displayName, '%')"
    }
]

export const UI_MESSAGES = {
    ALERT_ERROR_EXECUTING_QUERY: "Error executing the query:",
    ALERT_NO_RESULTS: "No results found",
    ALERT_QUERY_SUCCESS: "Query executed successfully with {number} results found",
    ALERT_SUCCESS_NO_VULNERABILITIES: "No vulnerabilities found!",
    APPS_FOUND_NUMBER: "\n{number} apps found.",
    BACKUP_NOTE: "Note: Backup will be created.",
    BUTTON_CANCEL: "Cancel",
    BUTTON_COLLECT: "Collect Local Software",
    BUTTON_COLLECT_ALL_CVES_PRODUCTS: 'Collect All CVEs and Affected Products',
    BUTTON_COLLECT_CVES_PRODUCTS: "Collect CVEs and Affected Products",
    BUTTON_COLLECT_CWES: "Collect CWEs",
    BUTTON_COLLECT_EXPLOITS: "Collect Exploits",
    BUTTON_DELETE: "DELETE",
    BUTTON_DISPLAY: "Display",
    BUTTON_DOWNLOAD: "Download",
    BUTTON_EXPORT: "Export",
    BUTTON_IMPORT: "Import",
    BUTTON_RUN: "Run",
    BUTTON_SCAN: "Scan",
    BUTTON_UPDATE: "Update Data (CVEs, Products)",
    COLLECTED_CVES_FOR_CWE: "Collected {total} CVEs for CWE-{cwe}.",
    COLLECTED_OUT_OF: "Collected {number} / {total} CVEs",
    COLLECTING_CVES_FOR_CWE: "Collecting CVEs for CWE-{cwe}...",
    COLLECTING_CVES: "Collecting All CVEs. This will take some time...",
    CONNECTION_ERROR: "Connection error. Please check your internet connection.",
    CVE_LIST_OWASP_NOTE: "CVEs displayed are included in OWASP's Crypto Failures CWEs category",
    CVE_SEARCH_PLACEHOLDER: "Search by CVE ID or Summary",
    CWE_TABLE_EMPTY: "CWE table is empty. Please collect CWEs and try again!",
    DEFAULT_DOWNLOAD_LOCATION_NOTE: "Default download location: browser downloads folder",
    DELETE_TABLE_LABEL: "Delete contents from tables:",
    DONE_WITH_CWE: "Done with CWE-{cwe}.\n",
    ERROR_429: "Error 429: Request limit reached!",
    ERROR_BACKUP: "An error occurred during backup: ",
    ERROR_FETCH: "An error occurred during fetch: ",
    ERROR_IMPORT: "An error occurred during import!",
    ERROR_LOG: "Error retrieving log: ",
    ERROR_LOG_RETRIEVAL: "An error occurred during fetch: ",
    EXAMPLE_QUERY_LIST: "Example Query List",
    EXPLOITS_FOUND: "\n{number} exploits found.",
    EXPLOIT_COLLECTOR_INITIAL_MSG: "Retrieving and storing exploits...\n",
    EXPORT_LABEL: "Export DB:",
    HEADER_EXPLOIT_DESCRIPTION: "The following exploits appear in exploit-db and are based on the products listed above (Product version may not be provided).",
    HEADER_POTENTIAL_EXPLOITS: "Potential Relevant Exploits",
    IMPORT_LABEL: "Import DB:",
    INFO_CVE_ADDITIONS: "New CVE additions made using the NVD recent json feed ",
    INFO_CVE_MODIFIED: "Modifications to existing CVEs made using the NVD modified json feed ",
    LABEL_DEVELOPER_GUIDE: "Developer Guide:",
    LABEL_USER_GUIDE: "User Guide:",
    LINK_CVES_DESCRIPTION: "The API used for collecting CVEs can be found here: ",
    LINK_CVES_SOURCE: "https://nvd.nist.gov/developers/vulnerabilities",
    LINK_CVES_SOURCE_HIGHLIGHT: "https://nvd.nist.gov/developers/vulnerabilities#CVE_API:~:text=1.0%20Product%20APIs.-,CVE%20API,-The%20CVE%20API",
    LINK_CWES_DESCRIPTION: "The CWEs are collected from ",
    LINK_CWES_SOURCE: "https://owasp.org/Top10/A02_2021-Cryptographic_Failures",
    LINK_EXPLOITS_DESCRIPTION: "Exploits are collected from ",
    LINK_EXPLOITS_SOURCE: "https://gitlab.com/exploit-database/exploitdb",
    LINK_MODIFIED_FEED: "Modified-Feed",
    LINK_MODIFIED_FEED_HIGHLIGHT: "https://nvd.nist.gov/vuln/data-feeds#:~:text=Size%20(MB)-,CVE%2DModified,-08/09/2023%3B%208",
    LINK_RECENT_FEED: "Recent-Feed",
    LINK_RECENT_FEED_HIGHLIGHT: "https://nvd.nist.gov/vuln/data-feeds#:~:text=1.09%20MB-,CVE%2DRecent,-08/09/2023",
    LOG_LABEL: "Log File:",
    MODAL_DELETE_CONTENTS: "All content from the tables selected will be deleted",
    NO_DATA_AVAILABLE: "No data available",
    PATH_WARNING_NOTE: "Note: Make sure the MySQL bin directory (e.g., C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin) is included in your PATH environment variable",
    PAUSE_FOR_10_MINUTES: "The script will pause for 10 minutes.",
    PLACEHOLDER_SEARCH_TEXT: "Search by ID or Description",
    QUERY_BOX_LABEL: "Query the database containing CWEs, CVEs, Products, Exploits and Installed Software",
    RESUME_AT: "The script will resume at {time}",
    SCANNER_INITIAL_MSG: "Initializing scanner...\n",
    SCANNING_SOFTWARE_AMOUNT: "Scanning {total} installed software for vulnerabilities...",
    SERVER_UNAVAILABLE: "The page is down or the server is unavailable. Please try again later.",
    SEVERITY_DESCRIPTION: "(Severity is based on cvss_v3 or cvss_v2 if cvss_v3 is not available)",
    SOFTWARE_COLLECTOR_INITIAL_MSG: "Retrieving installed software data...",
    SUCCESS_IMPORT: "Database imported successfully!",
    SUCCESS_LOG_RETRIEVED: "Log file retrieved successfully",
    TABLE_EXPLOIT_EMPTY: "Warning: Exploits table is empty. Please, run Exploit Collector!",
    TOTAL_CVES_DISPLAYED: "Total CVEs displayed",
    TOTAL_CVES_IMPORTED: "Total CVEs imported: {number}",
    TOTAL_CWES_COLLECTED: "\nTotal CWEs collected: ",
    TOTAL_EXPLOITS_DISPLAYED: "Total Exploits displayed",
    TOTAL_PAGES_FOR_CWE: "Total pages for CWE-{cwe}: {totalPages}",
    UPDATE_ADDED: "added.",
    UPDATE_ADDED_MODIFIED_MSG: "{cve} has been {action}\n",
    UPDATE_INITIAL_MSG: "Searching and Updating CVES...\n",
    UPDATE_MODIFIED: "modified (Note: if you already updated then no changes may be made).",
    UPDATE_WARNING: "\nWarning: If there's been more than 8 days since the last update a full data collect is suggested.",
    VULNERABILITIES_FOUND: "Found {total} vulnerabilities",
    WAIT_TO_IMPORT: "Importing CVEs into DB. Please wait...",
    WARNING_BACKUP_NOT_FOUND: "Error: Backup file not found",
    WARNING_INSTALLED_SOFTWARE_EMPTY: "Warning: Installed Software table is empty. Please, run Software Collector!",
    WARNING_NO_FILE_SELECTED: "No file selected",
    WARNING_PRODUCTS_EMPTY: "Warning: Products table is empty. Please, run CVE Collector!",
}