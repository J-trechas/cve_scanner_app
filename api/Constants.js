const USER_GUIDE_FILENAME = "User-guide.docx"
const DEV_GUIDE_FILENAME = "Dev-guide.docx"
const LOG_FILENAME = "combined.log"

module.exports = {
    USER_GUIDE_FILENAME,
    DEV_GUIDE_FILENAME,
    LOG_FILENAME
};