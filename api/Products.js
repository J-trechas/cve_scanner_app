const { getConnection } = require('./Database');
const logger = require('./logger');

const insertProductsDB = async (vendors, cve_id) => {
    var list = [];
    for (const vendorIndex in vendors) {
        const vendor = vendors[vendorIndex];
        for (const productIndex in vendor) {
            const product = vendor[productIndex];
            if (product.product)
                list = [...list, [null, product, vendorIndex, product.version, cve_id]]
        }
    }
    if (list.length == 0) return

    const connection = getConnection()

    // Insert products into DB
    let stmt = `INSERT INTO products VALUES ? ON DUPLICATE KEY UPDATE product=VALUES(product),version=VALUES(version)`;
    connection.query(stmt, [list], (err, results) => {
        if (err) {
            return logger.error(err.message);
        }
        logger.info(' Rows inserted in products:' + results.affectedRows)
    });
}

module.exports = {
    insertProductsDB,
};