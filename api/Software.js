const os = require('os');
const { exec } = require('child_process');
const { getConnection } = require('./Database');
const logger = require('./logger');
const fetchInstalledSoftware = require('fetch-installed-software');
const { getAllInstalledSoftware } = fetchInstalledSoftware;


const parseDate = (dateString) => {
    if (!dateString) return null


    let parts = [];
    if (!dateString.includes("/")) {
        parts = [dateString.slice(0, 4), dateString.slice(4, 6), dateString.slice(6)];
    } else {
        let regex = new RegExp(/(\d{1,2})\/(\d{1,2})\/([0-9]{4})/);
        let match = dateString.match(regex);
        if (match) {
            parts = [match[3], match[1].padStart(2, "0"), match[2].padStart(2, "0")];
        }
    }

    if (parts.length === 3) {
        return `${parts[0]}-${parts[1]}-${parts[2]}`;
    }


    return null;
};

const insertSoftwareDB = async (apps) => {
    const connection = getConnection()

    var list = [];
    var execTime = new Date()
    var displayNames = []
    for (const index in apps) {
        const app = apps[index];
        if (!app.DisplayName || displayNames.indexOf(app.DisplayName) > -1) continue

        displayNames = [...displayNames, app.DisplayName]
        let formattedInstallDate = parseDate(app.InstallDate)

        list = [...list, [app.DisplayName, app.DisplayVersion, app.Publisher, formattedInstallDate, execTime.toISOString(), execTime.toISOString(), 1]]

    }
    //Insert software into DB
    /* let stmt = `INSERT INTO installedsoftware VALUES ? ON DUPLICATE KEY UPDATE displayVersion=VALUES(displayVersion),lastModified=VALUES(lastModified),active=VALUES(active);`;
    connection.query(stmt, [list], (err, results) => {
        if (err) {
            return logger.error(err.message);
        }
        logger.info(' Rows inserted in installedSoftware:' + results.affectedRows)
    });
    //Update uninstalled sotfware in DB
    stmt = `UPDATE installedsoftware SET active =0 WHERE lastModified < "${execTime.toISOString()}" ;`;
    connection.query(stmt, (err, results) => {
        if (err) {
            return logger.error(err.message);
        }
        logger.info(' Rows set as uninstalled from installedSoftware:' + results.affectedRows)
    }); */
    const insertPromise = new Promise((resolve, reject) => {
        let stmt = `INSERT INTO installedsoftware VALUES ? ON DUPLICATE KEY UPDATE displayVersion=VALUES(displayVersion),lastModified=VALUES(lastModified),active=VALUES(active);`;
        connection.query(stmt, [list], (err, results) => {
            if (err) {
                logger.error(err.message);
                reject(err);
            }
            logger.info('Rows inserted in installedSoftware:' + results.affectedRows);
            resolve();
        });
    });

    const updatePromise = new Promise((resolve, reject) => {
        let stmt = `UPDATE installedsoftware SET active = 0 WHERE lastModified < "${execTime.toISOString()}" ;`;
        connection.query(stmt, (err, results) => {
            if (err) {
                logger.error(err.message);
                reject(err);
            }
            logger.info('Rows set as uninstalled from installedSoftware:' + results.affectedRows);
            resolve();
        });
    });

    // Execute the Promises in sequence
    try {
        await insertPromise;
        await updatePromise;
        // Both queries completed, you can perform any follow-up actions here if needed
    } catch (error) {
        logger.error('An error occurred:', error);
    }

}

const getLinuxInstalledSoftware = async () => {
    return new Promise((resolve, reject) => {
        exec('dpkg-query -W --showformat=\'${Package}\t${Version}\t${Maintainer}\n\'', (error, stdout, stderr) => {
            if (error) {
                reject(error);
            } else {
                const softwareList = stdout.split('\n')
                    .filter((item) => item)
                    .map((line) => {
                        const [name, version, maintainer] = line.split('\t');
                        return {
                            name,
                            version,
                            publisher: maintainer
                        };
                    });
                resolve(softwareList);
            }
        });
    });
};

const getInstalledSoftwareWMI = () => {
    return new Promise((resolve, reject) => {
        exec('wmic product get Name, Version, Vendor, InstallDate', (err, stdout, stderr) => {
            if (err) {
                reject(err);
                return;
            }

            // Process the output to get the list of installed software names
            const installedSoftwareOnPC = stdout
                .split('\n')
                .slice(1) // Skip the header row
                .filter(Boolean) // Remove empty lines
                .map(line => line.split(/\s\s+/).slice(0, -1)) // Split by multiple spaces
                .map(app => {
                    const transformed = {
                        DisplayName: app[1],
                        DisplayVersion: app[3],
                        Publisher: app[2],
                        InstallDate: app[0]
                    };
                    return transformed;
                });

            resolve(installedSoftwareOnPC);
        });
    });
}

// Create a function to get the local installed software
const getLocalInstalledSoftware = async (store = false) => {
    const platform = os.platform();

    if (platform === 'win32') {
        var apps = await getAllInstalledSoftware();

        /* const apps = await getInstalledSoftwareWMI(); */ //Get software using WMI

        if (store) {
            await insertSoftwareDB(apps);
        }

        return apps;
    } else if (platform === 'linux') {
        var softwareList = await getLinuxInstalledSoftware();
        return softwareList;
    } else {
        throw new Error('The current platform is not supported.');
    }
};



module.exports = {
    getLocalInstalledSoftware,
};