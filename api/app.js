// Import required modules
const express = require('express');
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const cors = require('cors'); // Import cors module
const { getLocalInstalledSoftware } = require('./Software'); // import the function from Software.js
const { getConnection, CONNECTION_OPTIONS } = require('./Database');
const { parseExploits } = require('./Exploits');
const { checkCveUpdates } = require('./UpdateCVE');
const {USER_GUIDE_FILENAME, LOG_FILENAME, DEV_GUIDE_FILENAME } = require('./Constants');
const logger = require('./logger');
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const mysqldump = require('mysqldump');
const { spawn, exec } = require('child_process');
const path = require('path');
const multer = require('multer');
const fs = require('fs');
const mammoth = require("mammoth");





// Initialize the app
const app = express();

// Enable CORS for all routes
app.use(cors({ origin: 'http://localhost:3000' }));


// Configure body-parser
// Set the maximum allowed size for request entities to 50MB
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

//Connect to DB
const connection = getConnection()

// Create the endpoint for executing a query
app.post('/execute-query', (req, res) => {
    const query = req.body.query;

    if (!query) {
        return res.status(400).json({ message: 'Query is required.' });
    }

    connection.query(query, (err, results) => {
        if (err) {
            logger.error('Error executing the query:' + err);
            return res.status(500).json({ message: 'Error executing the query.', error: err.message });
        }
        res.status(200).json({ message: 'Query executed successfully.', results });
        logger.info('Query: `' + query + '` executed successfully')
    });
});

// Endpoint to execute INSERT query
app.post('/insert-query', async (req, res) => {
    const { query, values } = req.body;

    try {
        // Execute query with provided values
        connection.query(query, [values], (err, results) => {

            if (err) {
                logger.error('Error executing the query:' + err)
                return res.status(500).json({ message: 'Error executing the query.', error: err.message });
            }

            res.status(200).json({ message: 'Query executed successfully.', results });
        });
    } catch (error) {
        logger.error('Error connecting to the database:' + error)
        return res.status(500).json({ message: 'Error connecting to the database.', error: error.message });
    }
});

// Function to create GET endpoints for SELECT * queries
const createSelectAllEndpoint = (path, tableName) => {
    app.get(path, (req, res) => {
        connection.query(`SELECT * FROM ${tableName}`, (err, results) => {
            if (err) {
                logger.error(`Error executing the query for ${tableName}:${err}`)
                return res.status(500).json({ message: `Error executing the query for ${tableName}.`, error: err.message });
            }
            res.status(200).json({ message: `Query executed successfully for ${tableName}.`, results });
            logger.info(`Query executed successfully for ${tableName}.`)
        });
    });
};

// Create GET endpoints for each table
createSelectAllEndpoint('/cwe', 'cwe');
createSelectAllEndpoint('/cve', 'cve');
createSelectAllEndpoint('/exploits', 'exploits');
createSelectAllEndpoint('/installedsoftware', 'installedsoftware');
createSelectAllEndpoint('/products', 'products');

// Create an endpoint to return the installed apps
app.get('/local-installed-software', async (req, res) => {
    try {
        const store = req.query.store === 'true';
        const apps = await getLocalInstalledSoftware(store);
        res.status(200).json({ message: 'Fetched installed apps successfully.', apps });
        logger.info('Fetched installed apps successfully.')
    } catch (error) {
        logger.error('Error fetching installed apps:' + error)
        res.status(500).json({ message: 'Error fetching installed apps.', error: error.message });
    }
});

app.get('/exploits-data', async (req, res) => {
    try {
        const data = await parseExploits();
        res.status(200).json({ message: 'Fetched exploits successfully.', data });
        logger.info('Fetched exploits successfully.')
    } catch (error) {
        logger.error(error)
        res.status(500).json({ error: 'Failed to fetch exploits data' });
    }
});

app.get('/update-cve', async (req, res) => {
    try {
        const data = await checkCveUpdates();
        res.status(200).json({ message: 'CVEs updated successfully.', data });
        logger.info('CVEs updated successfully.')
    } catch (error) {
        logger.error(error)
        res.status(500).json({ error: 'Failed to update CVEs' });
    }
});



/* app.post('/fetch', async (req, res) => {
    const { url, apiKey, username, password } = req.body;

    try {
        const encodedCredentials = Buffer.from(`${username}:${password}`).toString('base64');
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': 'Basic ' + encodedCredentials,
                'apiKey': apiKey
            },
        });
        let data = ""
        try {
            data = await response.json();
        } catch (error) {
            logger.error(error);
        }
        res.status(200).json({ message: 'Data fetched successfully.', data, status: response.status });
        logger.info(`Fetch request successfull: ${url}`)
    } catch (error) {
        logger.error('Error fetching data:' + error);
        return res.status(500).json({ message: 'Error fetching data.', error: error.message, status: error.status });
    }
}); */

// Helper function to delay execution
function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
app.post('/fetch', async (req, res) => {
    const { url, apiKey } = req.body;
    const maxRetries = 2;
    const retryDelay = 5000; // 5 seconds

    try {
        let response;
        let data;

        for (let retry = 0; retry <= maxRetries; retry++) {
            try {
                response = await fetch(url, {
                    method: 'GET',
                    headers: {
                        'apiKey': apiKey
                    },
                });

                data = await response.json();
                break; // Break out of the retry loop if successful
            } catch (error) {
                logger.error(error);

                if (retry < maxRetries) {
                    logger.error(`Fetch request failed.Trying again`)
                    await delay(retryDelay); // Delay before retrying
                } else {
                    throw error; // Throw the error if max retries exceeded
                }
            }
        }

        res.status(200).json({ message: 'Data fetched successfully.', data, status: response.status });
        logger.info(`Fetch request successful: ${url}`);
    } catch (error) {
        logger.error('Error fetching data:' + error);
        return res.status(500).json({ message: 'Error fetching data.', error: error.message, status: error.status });
    }
});



app.get('/downloadDB', (req, res) => {
    const database = CONNECTION_OPTIONS.database;
    const dumpFileName = `${database}.sql`;

    const mysqldump = spawn('mysqldump', [
        '-u',
        CONNECTION_OPTIONS.user,
        `-p${CONNECTION_OPTIONS.password}`,
        database,
    ]);

    res.setHeader('Content-Disposition', `attachment; filename=${dumpFileName}`);
    mysqldump.stdout.pipe(res);

    mysqldump.on('close', (code) => {
        if (code === 0) {
            logger.info('Database exported successfully');
        } else {
            logger.error('Error exporting database');
            res.status(500).send('An error occurred');
        }
    });
});

const backupFolder = path.join(__dirname, 'backups'); // Define the backup folder path
const maxBackups = 5; // Maximum number of backups to keep

app.get('/backupDB', (req, res) => {
    const database = CONNECTION_OPTIONS.database;
    const timestamp = new Date().toISOString().replace(/[-:.]/g, '_'); // Generate a timestamp for the backup filename

    const backupFileName = `${database}_${timestamp}.sql`;
    const backupFilePath = path.join(backupFolder, backupFileName);

    // Create the backup folder if it doesn't exist
    if (!fs.existsSync(backupFolder)) {
        fs.mkdirSync(backupFolder);
    }

    // Get the list of existing backups
    let existingBackups = fs.readdirSync(backupFolder);
    existingBackups = existingBackups.filter(fileName => fileName.endsWith('.sql'));

    // Check the number of existing backups
    if (existingBackups.length >= maxBackups) {
        // Sort the backups by filename (ascending) based on timestamp
        existingBackups.sort();

        // Delete the oldest backup file
        const oldestBackupFile = path.join(backupFolder, existingBackups[0]);
        fs.unlinkSync(oldestBackupFile);

        logger.info('Oldest backup deleted: ' + existingBackups[0]);
    }

    const command = `mysqldump -u ${CONNECTION_OPTIONS.user} -p${CONNECTION_OPTIONS.password} ${database} > "${backupFilePath}"`;

    exec(command, (error, stdout, stderr) => {
        if (error) {
            logger.error('Error creating backup:' + error);
            res.status(500).json({ message: 'Error creating backup', error: error.message });
        } else {
            logger.info('Backup created successfully: ' + backupFileName);
            const backupFile = fs.readFileSync(backupFilePath, 'utf-8');
            res.status(200).json({ message: 'Backup created successfully', backupFileName, backupFile });
        }
    });
});

const upload = multer({ dest: 'uploads/' }); // this is where the uploaded .sql files will be stored

app.post('/uploadDB', upload.single('dbDump'), (req, res) => {
    const database = CONNECTION_OPTIONS.database;
    const dumpFileName = req.file.path;

    const command = `mysql -u${CONNECTION_OPTIONS.user} -p${CONNECTION_OPTIONS.password} ${database} < ${dumpFileName}`;
    exec(command, (error, stdout, stderr) => {
        // delete the file
        fs.unlink(dumpFileName, (err) => {
            if (err) {
                logger.error(`Deleting file: ${err}`);
            } /* else {
                logger.log(`File deleted: ${dumpFileName}`);
            } */
        });

        if (error) {
            logger.error(error.message);
            res.status(500).json({ message: 'An error occurred', error: error.message });
        } else {
            logger.info('Database imported successfully');
            res.status(200).json({ message: 'Database imported successfully' });
        }
    });
});

app.get('/guide', (req, res) => {
    const useParam = req.query.use; // Extract 'use' query parameter
    const filename = useParam == 'dev' ? DEV_GUIDE_FILENAME : USER_GUIDE_FILENAME;
    const rel_path = '/Docs/'
    const filePath = path.join(__dirname, rel_path, filename);

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    res.setHeader('Content-Disposition', `attachment; filename=${filename}`);

    res.sendFile(filePath, (err) => {
        if (err) {
            logger.error(err);
            res.status(500).send('An error occurred while trying to send the user guide.');
        } else {
            logger.info('User guide sent successfully.');
        }
    });
});
app.get('/log', (req, res) => {
    const filename = LOG_FILENAME;
    const filePath = path.join(__dirname, filename);

    res.setHeader('Content-Type', 'text/plain');
    res.setHeader('Content-Disposition', `attachment; filename=${filename}`);

    res.sendFile(filePath, (err) => {
        if (err) {
            logger.error(err);
            res.status(500).send('An error occurred while trying to send the log file.');
        } else {
            logger.info('Log file sent successfully.');
        }
    });
});

app.get('/log_raw', (req, res) => {
    const filename = LOG_FILENAME;
    const filePath = path.join(__dirname, filename);

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({ message: 'An error occurred while trying to read the log file.', error: err.message });
        } else {
            logger.info('Log file read successfully.');
            res.json({ log: data });
        }
    });
});

app.get('/log_raw', (req, res) => {
    const filename = LOG_FILENAME;
    const filePath = path.join(__dirname, filename);

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({ message: 'An error occurred while trying to read the log file.', error: err.message });
        } else {
            logger.info('Log file read successfully.');
            res.json({ log: data });
        }
    });
});


app.get('/guide_raw', (req, res) => {
    const useParam = req.query.use; // Extract 'use' query parameter
    const filename = useParam == 'dev' ? DEV_GUIDE_FILENAME : USER_GUIDE_FILENAME;
    const rel_path = '/Docs/'
    const filePath = path.join(__dirname, rel_path, filename);

    fs.readFile(filePath, (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({ message: `An error occurred while trying to read the ${filename} file.`, error: err.message });
        } else {
            logger.info(filename + ' read successfully.');

            mammoth.convertToHtml({ path: filePath })
                .then(result => {
                    const html = result.value;
                    res.setHeader('Content-Type', 'text/html');
                    res.send(html);
                })
                .catch(error => {
                    logger.error(error);
                    res.status(500).json({ message: `An error occurred while processing the ${filename} file.`, error: error.message });
                });
        }
    });
});




// Start the server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    logger.info(`Server started running on port ${PORT}`);
});

module.exports = {
    connection
};