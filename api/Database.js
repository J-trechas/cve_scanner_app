


const mysql = require('mysql2');
const logger = require('./logger');

const CONNECTION_OPTIONS = {
    host: "localhost",
    user: "csd4172",
    password: "password",
    database: "CryptographicIssues"
}

const getConnection = () => {

    // MySQL connection configuration
    const connection = mysql.createConnection(CONNECTION_OPTIONS);

    connection.connect((err) => {
        if (err) {
            logger.error('Error connecting to the database:' + err)
            return;
        }
    });

    return connection
}

module.exports = {
    getConnection,
    CONNECTION_OPTIONS
};
