const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));


// Function to retrieve CPEs for a CVE
async function retrieveCPEsForCVE(cveId) {
    try {
        const url = `https://services.nvd.nist.gov/rest/json/cves/2.0?cveId=${cveId}`;
        const response = await fetch(url);
        const cveData = await response.json();
        /* console.log(cveData.vulnerabilities[0].cve.configurations?.[0].nodes) */
        const vendors = {};
        const configs = cveData?.vulnerabilities?.[0].cve?.configurations ?? null;
        if (configs && configs.length > 0) {
            configs.forEach(config => {
                let nodes = config.nodes
                if (nodes && nodes.length > 0) {
                    nodes.forEach(node => {
                        if (node.cpeMatch && node.cpeMatch.length > 0) {
                            const cpes = node.cpeMatch;
                            cpes.forEach(cpe => {
                                let cpeMatch = cpe.criteria.split(':')
                                const vendor = cpeMatch[3];
                                const product = cpeMatch[4];
                                let version = cpeMatch.length > 5 ? cpeMatch[5] : null;
                       
                                if (cpe.versionStartIncluding || cpe.versionEndExcluding || cpe.versionEndIncluding) {
                                    const startVersion = cpe?.versionStartIncluding ?? "0";
                                    const endVersion = cpe?.versionEndExcluding ?? cpe?.versionEndIncluding ?? "now";
                                    version = `${startVersion} up_to ${endVersion}`;
                                }

                                if (vendor in vendors) {
                                    vendors[vendor].push({ product, version });
                                } else {
                                    vendors[vendor] = [{ product, version }];
                                }
                            })
                        }
                    })
                }
            }
            )
        }
        console.log(vendors)
        return cpes;
    } catch (error) {
        console.error('Error retrieving CPEs:', error.message);
        return [];
    }
}

// Usage example
const cveId = 'CVE-2022-35255';
retrieveCPEsForCVE(cveId)
    .then(cpes => {
        console.log('CPEs for', cveId, ':', cpes);
    })
    .catch(error => {
        console.error('Error:', error);
    });
/* 
async function retrieveMatchingCPEs(cpe) {
    try {
        const url = `https://services.nvd.nist.gov/rest/json/cpes/1.0?cpeMatchString=${encodeURIComponent(cpe)}`;
        const response = await fetch(url);
        const responseData = await response.json();

        // Extract the matching CPEs
        const matchingCPEs = responseData.result.cpes || [];

        return matchingCPEs;
    } catch (error) {
        console.error('Error retrieving matching CPEs:', error.message);
        return [];
    }
}

// Usage example
const cpeString = 'cpe:2.3:a:nodejs:node.js:*:*:*:*:*:*:*:*';

retrieveMatchingCPEs(cpeString)
    .then(matchingCPEs => {
        console.log('Matching CPEs:', matchingCPEs);
    })
    .catch(error => {
        console.error('Error:', error);
    }); */