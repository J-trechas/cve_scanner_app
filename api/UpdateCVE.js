const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const zlib = require('zlib');
const { insertProductsDB } = require('./Products');
const fs = require('fs');
const { getConnection } = require('./Database');
const logger = require('./logger');


// Function to check for updates on CVEs
async function checkCveUpdates() {

    const connection = getConnection()

    let CVEsUpdated = []
    const cweIdsToMonitor = [];
    let query = 'SELECT id FROM cwe;';
    connection.query(query, (error, results, fields) => {
        if (error) {
            logger.error(`Failed to retrieve list of CWE IDs to monitor: ${error}`);
            return;
        }
        results.forEach((row) => {
            cweIdsToMonitor.push(row.id);
        });
        logger.info(`Retrieved ${cweIdsToMonitor.length} CWE IDs to monitor.`)
    });
    query = 'SELECT id FROM cve;';
    const cve_list = [];
    connection.query(query, (error, results, fields) => {
        if (error) {
            logger.error(`Failed to retrieve list of CVE IDs: ${error}`);
            return;
        }
        results.forEach((row) => {
            cve_list.push(row.id);
        });
    });
    try {
        // Get NVD data
        let response = await fetch('https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.gz');
        let compressedData = await response.arrayBuffer();
        let data = JSON.parse(zlib.gunzipSync(compressedData));
        response = await fetch('https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-modified.json.gz');
        compressedData = await response.arrayBuffer();
        
        data.CVE_Items = [...data.CVE_Items, ...JSON.parse(zlib.gunzipSync(compressedData)).CVE_Items];
        


        // Loop through CVE items
        Object.keys(data.CVE_Items).forEach(cveId => {
            const cveData = data.CVE_Items[cveId];
            //Extract CWE ids
            const cweIds = cveData.cve.problemtype?.problemtype_data?.[0]?.description?.map(desc => parseInt(desc.value.match(/CWE-(\d+)/)?.[1])) ?? [];
            //console.log(cveData)

            if (cweIdsToMonitor.some(cweId => cweIds.includes(cweId))) {


                // Object to store vendors and their products
                const vendors = {};
                // Update the CVE in the database
                const cveId = cveData.cve.CVE_data_meta.ID;
                const summary = cveData.cve.description.description_data[0].value;
                const cvssV2 = cveData.impact?.baseMetricV2?.cvssV2?.baseScore ?? null;
                const cvssV3 = cveData.impact?.baseMetricV3?.cvssV3?.baseScore ?? null;
                const productsAffected = cveData.configurations?.nodes?.map(node => node.cpe_match?.[0]?.cpe23Uri).filter(uri => uri !== undefined).length;
                const datePublished = cveData.publishedDate;
                // Extract vendor and product data
                const configs = cveData?.configurations?.nodes;
                if (configs && configs.length > 0) {
                    configs.forEach(config => {
                        let nodes = config.nodes
                        if (nodes && nodes.length > 0) {

                            nodes.forEach(node => {
                                if (node.cpeMatch && node.cpeMatch.length > 0) {
                                    const cpes = node.cpeMatch;

                                    cpes.forEach(cpe => {
                                        let cpeMatch = cpe.criteria.split(':')
                                        const vendor = cpeMatch[3];
                                        const product = cpeMatch[4];
                                        let version = cpeMatch.length > 5 ? cpeMatch[5] : null;

                                        if (cpe.versionStartIncluding || cpe.versionStartExcluding || cpe.versionEndExcluding || cpe.versionEndIncluding) {
                                            const startVersion = cpe?.versionStartExcluding ?? cpe?.versionStartIncluding ?? "0";
                                            const endVersion = cpe?.versionEndExcluding ?? cpe?.versionEndIncluding ?? "now";
                                            version = `${startVersion} up_to ${endVersion}`;
                                        }
                                        if (vendor in vendors) {
                                            vendors[vendor].push({ product, version });
                                        } else {
                                            vendors[vendor] = [{ product, version }];
                                        }
                                    })
                                }
                            })
                        }
                    }
                    )
                }

                cweIds.forEach(cweId => {
                    if (!cweIdsToMonitor.includes(cweId)) return

                    const params = [cveId, summary, cweId, productsAffected, cvssV2, cvssV3, datePublished];
                    CVEsUpdated.push({
                        id: cveId,
                        summary,
                        cwe_id: cweId,
                        products_affected: productsAffected,
                        cvss_v2: cvssV2,
                        cvss_v3: cvssV3,
                        date_published: datePublished
                    })

                    const insertQuery = `INSERT INTO cve (id, summary, cwe_id, products_affected, cvss_v2, cvss_v3, date_published)
                        VALUES (?, ?, ?, ?, ?, ?, ?)
                        ON DUPLICATE KEY UPDATE
                        id = VALUES(id),
                        summary=VALUES(summary),
                        products_affected=VALUES(products_affected),
                        cvss_v2=VALUES(cvss_v2),
                        cvss_v3=VALUES(cvss_v3),
                        date_published=VALUES(date_published)
                        `;

                    connection.query(insertQuery, params, (error, results, fields) => {
                        if (error) {
                            logger.error(`Failed to add or modify CVE ${cveId}: ${error}`);
                            return;
                        }
                        logger.info(`CVE ${cveData.cve.CVE_data_meta.ID} has been ${cve_list.includes(cveData.cve.CVE_data_meta.ID) ? "modified" : "added"}`)
                    });


                })
                insertProductsDB(vendors, cveId)

            }
        });

    } catch (error) {
        logger.error(`Failed to retrieve NVD data: ${error}`);
    }
    return CVEsUpdated
}



module.exports = {
    checkCveUpdates,
};