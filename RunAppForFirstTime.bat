@echo off

echo === Starting Frontend ===
start cmd /k "npm install --force & npm start"

echo === Starting Backend ===
start cmd /k "cd ./api & npm install & node app.js"

echo === Main batch script completed. ===
