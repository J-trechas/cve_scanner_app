@echo off

echo === Starting Frontend ===
start cmd /k "npm start"

echo === Starting Backend ===
start cmd /k "cd ./api & node app.js"

echo === Main batch script completed. ===
